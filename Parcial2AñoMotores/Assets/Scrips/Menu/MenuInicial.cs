using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Cancion()
    {
        SceneManager.LoadScene(1);
    }

    public void Manejo()
    {
        SceneManager.LoadScene(2);
    }

    public void Juego1()
    {
        SceneManager.LoadScene(3);
    }

    public void Juego2()
    {
        SceneManager.LoadScene(4);
    }

    public void Salir()
    {
        Debug.Log("Salir.");
    }
}

