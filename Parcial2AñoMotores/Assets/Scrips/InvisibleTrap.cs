using UnityEngine;

public class InvisibleTrap : MonoBehaviour
{
    public GameObject player;
    public GameObject trap;

    public float activationDistance = 2f; // Distancia a la que la trampa se activar�

    private Renderer trapRenderer;
    private bool trapActivated = false;

    private void Start()
    {
        trapRenderer = trap.GetComponent<Renderer>();
        trapRenderer.enabled = false;
    }

    private void Update()
    {
        if (player != null && trap != null)
        {
            // Calcula la distancia entre el jugador y la plataforma de la trampa
            float distance = Vector3.Distance(player.transform.position, transform.position);

            if (distance < activationDistance && !trapActivated)
            {
                ActivateTrap();
            }
            else if (distance >= activationDistance && trapActivated)
            {
                DeactivateTrap();
            }
        }
    }

    private void ActivateTrap()
    {
        if (trapRenderer != null)
        {
            trapRenderer.enabled = true;
            trapActivated = true;
        }
    }

    private void DeactivateTrap()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
