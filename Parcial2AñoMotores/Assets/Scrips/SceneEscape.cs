using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneEscape : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LoadFirstScene();
        }
    }

    private void LoadFirstScene()
    {
        SceneManager.LoadScene(0); // Carga la primera escena en el �ndice 0
    }
}

