using System.Collections;
using UnityEngine;

public class FallingObject : MonoBehaviour
{
    public Transform initialPosition;
    public float fallSpeed = 10f;
    public float resetDelay = 2f;
    public float detectionDistance = 5f; // Distancia para detectar el objeto y hacer que caiga

    private Rigidbody rb;
    private bool hasFallen = false;
    private bool hasReset = false; // Variable para controlar si el objeto ha sido reseteado
    private Vector3 startPosition;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        startPosition = transform.position;
    }

    private void Update()
    {
        if (!hasFallen && !hasReset)
        {
            DetectObject();
        }
    }

    private void DetectObject()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, detectionDistance))
        {
            if (hit.transform.CompareTag("Player")) // Puedes cambiar la etiqueta seg�n tus necesidades
            {
                Fall();
            }
        }
    }

    private void Fall()
    {
        if (rb != null)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
            hasFallen = true;

            StartCoroutine(ResetObject());
        }
    }

    private IEnumerator ResetObject()
    {
        yield return new WaitForSeconds(resetDelay);

        if (rb != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        hasReset = true; // Marcar el objeto como reseteado
        transform.position = startPosition;
    }
}
