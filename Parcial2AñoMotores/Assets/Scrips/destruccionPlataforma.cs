using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destruccionPlataforma : MonoBehaviour
{
    [SerializeField] private float tiempoEspera;
    private Rigidbody rb;
    [SerializeField] private float velocidadRotacion;
    public bool caida = false;
    public float n = 0;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            caida = true;


        }   
    }

    private void Update()
    {
        if (caida == true)
        {
            
            n += Time.deltaTime * 1;
            if (n > tiempoEspera)
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
    }
    public IEnumerator Caida (Collision other)
    {
        yield return new WaitForSeconds(tiempoEspera);
        caida = true;
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), other.transform.GetComponent<Collider>());
        rb.constraints = RigidbodyConstraints.None;
        rb.AddForce(new Vector3(0.1f, 0));


    }
}
