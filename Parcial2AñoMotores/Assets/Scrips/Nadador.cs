using UnityEngine;

public class Nadador : MonoBehaviour
{
    private bool isSwimming = false;
    private float swimSpeed = 4f;
    private float waterLevel = 0f;
    private float gravity = 100f;

    private Rigidbody rb;
    private Renderer playerRenderer;
    private Color originalColor;
    public Color swimColor = Color.cyan;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerRenderer = GetComponent<Renderer>();
        originalColor = playerRenderer.material.color;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Agua"))
        {
            isSwimming = true;
            playerRenderer.material.color = swimColor;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Agua"))
        {
            isSwimming = false;
            playerRenderer.material.color = originalColor;
        }
    }

    private void FixedUpdate()
    {
        if (isSwimming)
        {
            // Obtener la entrada de movimiento en los ejes horizontal y vertical
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            // Calcular la direcci�n de movimiento
            Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0f);

            // Normalizar la direcci�n y aplicar la velocidad de nado
            movement = movement.normalized * swimSpeed;

            // Aplicar el movimiento al personaje
            rb.velocity = movement;

            // Aplicar fuerza de gravedad hacia abajo
            rb.AddForce(Vector3.down * gravity, ForceMode.Acceleration);
        }
    }
}
