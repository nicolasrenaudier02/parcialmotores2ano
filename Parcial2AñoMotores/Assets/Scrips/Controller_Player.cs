using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Controller_Player : MonoBehaviour
{
    public float velocidadMovimiento = 5f;
    public float fuerzaSalto = 5f;
    public int maxSaltos = 2;
    

    private bool puedeSaltar = true;
    private int saltosRealizados = 0;
    private Rigidbody rb;
    private bool enSuelo = false;
   
    

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // Movimiento horizontal
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        rb.velocity = new Vector3(movimientoHorizontal * velocidadMovimiento, rb.velocity.y, 0f);

        // Salto
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (puedeSaltar)
            {
                rb.velocity = new Vector3(rb.velocity.x, fuerzaSalto, 0f);
                saltosRealizados++;
                enSuelo = false;
                puedeSaltar = false;
            }
            else if (saltosRealizados < maxSaltos - 1)
            {
                rb.velocity = new Vector3(rb.velocity.x, fuerzaSalto, 0f);
                saltosRealizados++;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        string scenaActual = SceneManager.GetActiveScene().name;

        if (collision.gameObject.CompareTag("Suelo"))
        {
            enSuelo = true;
            saltosRealizados = 0;
            puedeSaltar = true;
        }

        if (collision.gameObject.CompareTag("DeathZone"))
        {
            // Destruir el objeto enemigo
            Destroy(this.gameObject);
            // Cambiar a otra escena (reemplaza "NombreDeLaEscena" con el nombre de la escena a la que deseas cambiar)
            SceneManager.LoadScene(scenaActual);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Suelo"))
        {
            enSuelo = false;
        }
    }
   

}
